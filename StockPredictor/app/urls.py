from django.urls import path
from . import views

from .views import ChartData

urlpatterns = [
    path('', views.index, name='index'),
    path('register/', views.register, name='register'),
    path('about/', views.about, name='about'),
    path('contact/', views.contact, name='contact'),
    path('api/chart/data/', ChartData.as_view()),
    path('search/', views.get_name, name='get_name'),
    path('search/', views.search, name='search'),
]